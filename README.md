# 使用Python编写一个反向代理脚本，使学校服务器上的特定端口可以接收外部请求


# 这个脚本使用了Python的内置http.server模块来创建一个简单的HTTP服务器，
# 它接收来自外部客户端的请求，然后将这些请求代理到学校服务器上的指定端口。
# 确保你修改了school_server_address变量，以匹配学校服务器上的实际地址和端口。
# 请注意，这种方法会在代理服务器上创建一个反向代理，从而使外部请求能够访问学校服务器上的应用程序。
# 但是，这仍然需要合法的网络配置和端口打开权限。确保在实施之前与学校的网络管理员协商，以确保你的操作合法并得到允许。
# 此外，需要定期监控和维护这个代理服务器，以确保安全性和性能。



import http.server
import socketserver

# 指定学校服务器上的端口和地址
school_server_address = ('localhost', 8080)  # 这里假设学校服务器监听在本地地址的8080端口

# 指定反向代理监听的地址和端口，这将成为外部访问的入口
proxy_server_address = ('0.0.0.0', 80)  # 0.0.0.0表示监听所有网络接口，端口80是HTTP默认端口

class ProxyHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.proxy_request()

    def proxy_request(self):
        import http.client

        # 建立与学校服务器的连接
        school_server = http.client.HTTPConnection(school_server_address[0], school_server_address[1])
        school_server.request(self.command, self.path, self.headers)
        school_response = school_server.getresponse()

        # 将学校服务器的响应发送回客户端
        self.send_response(school_response.status)
        for header, value in school_response.getheaders():
            self.send_header(header, value)
        self.end_headers()
        self.copyfile(school_response, self.wfile)

if __name__ == '__main__':
    with socketserver.TCPServer(proxy_server_address, ProxyHandler) as httpd:
        print(f"反向代理服务器正在监听端口 {proxy_server_address[1]}")
        httpd.serve_forever()
